Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.

Ispravljeni kod:

fname = input('Enter the file name: ')
try:
    fhand = open(fname)
except:
    print ('File cannot be opened: ', fname)
    exit()

counts = dict()
for line in fhand:
    words = line.split()
   
    for word in words:
        if word not in counts:
            counts[word] = 1
        else:
            counts[word] += 1
print (counts)


Promjene:

input umjesto raw_input
print ('File cannot be opened: ', fname) sa zagradama umjesto bez zagrada
fname umjeto fnamex
counts[word] +=1 umjesto counts[word] = 1 u predzadnjoj liniji
print (counts) sa zagradama umjesto bez zagrada 


OPIS VJEŽBE:

U ovoj vježbi smo učili povezivanje na gitLab, kloniranje projekta s gitLaba te spremanje promjena na gitLab.
Pravili smo grane koje se povezuju s glavnom granom i za svaki zadatak smo morali napraviti posebnu granu.
U prvom zadatku se omogućio unos podataka te njihov ispis na ekranu.
U drugom zadatku se pomoću try except petlje provjeravalo kojem opsegu pripada unesen broj te se ovisno o tome
slao podatak za ispis na ekranu.
U trećem zadatku je bilo potrebno nadograditi prvi zadatak s funkcion Calculator u kojoj je definiran način na
koji se množe uneseni podaci te se ta funkcija poziva nakon unesenih podataka pa se opet rješenje ispisuje na ekran.
U četvrtom zadatku se provjerava je li unos broj te ako jest unosi se određen broj brojeva dok se ne upiše ključna riječ
"Done" nakon koje prestaje unos i računaju se i ispisuju broj unesenih brojeva, njihova srednja vrijednost, te minimalna
i maksimalna vrijednost.
U petom zadatku smo omogućili čitanje iz tekstualnih datoteka te pomoću funkcije startswith pronađeni su svi brojevi koji
se nalaze iza ključne riječi te se računao prosjek tih vrijednosti.
U šestom zadatku je također bilo potrebno otvoriti tekstualnu datoteku i koristiti funkciju startswith. Korištena je
i metoda split koja razdvaja string tamo gdje je određeno podudaranje. Te smo vratili broj tih stringova.