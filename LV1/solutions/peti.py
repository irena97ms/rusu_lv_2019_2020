# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 18:10:38 2020

@author: irena
"""

myfile = open("D:/rusu_lv_2019_2020/LV1/resources/mbox.txt","r")
#myfile = open("D:/rusu_lv_2019_2020/LV1/resources/mbox-short.txt","r")
count = 0
average = 0
for line in myfile:
    if not line.startswith("X-DSPAM-Confidence:") : continue
    average += float(line[20:-1].strip())
    count = count + 1
    print(line)
    
print("Average spam confidence:", (average/count))