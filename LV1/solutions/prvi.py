# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 17:33:58 2020

@author: irena
"""
class NumberNotInRangeError(Exception):
    
     def _init_(self,x, message="Nije u intervalu"):
         self.x=x
         self.message=message
         super()._init_(self.message)
         pass

while True :
    try:
        x = float (input ("Unesite ocjenu: "))
        if not 0 < x <1:
            raise NumberNotInRangeError(x)
        if  x >= 0.9:
            print("A")
            break
        if  x >= 0.8:
            print("B")
            break
        if x >= 0.7:
            print("C")
            break
        if x >= 0.6:
            print("D")
            break
        if x < 0.6:
            print("F")
            break
        break 
    except NumberNotInRangeError:
        print("Ocijena nije u intervalu od 0.0 do 1.0")
    except ValueError:
        print("Niste unijeli broj")