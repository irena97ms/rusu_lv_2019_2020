# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 18:03:27 2020

@author: irena
"""

num = 0
tot = 0.0
list1 = []
while True:
    number = input("Enter a number: ")
    if number == 'Done':
        break
    try :
        num1 = float(number)
    except:
        print('Niste unijeli broj')
        continue
    list1.append(number)
    num = num+1
    tot = tot + num1
    Min = min(list1)
    Max = max(list1)
print("broj unesenih brojeva: ", num)
print("srednja vrijednost: ", tot/num)
print("minimalna vrijednost: ", Min)
print("maksimalna vrijednost: ", Max)