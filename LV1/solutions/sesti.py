# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 21:54:17 2020

@author: irena
"""

import re

file = open("D:/rusu_lv_2019_2020/LV1/resources/mbox-short.txt", "r")

hosts = []

for line in file:
    if line.startswith("From"):
        match = re.search(r'[\w\.-]+@[\w\.-]+', line)
        hosts.append(match.group(0))

dictionary = dict()
for i in hosts:
    domain = i.split('@')[1]
    if domain not in dictionary:
        dictionary[domain] = 1
    else:
        dictionary[domain] +=1

print(dictionary)