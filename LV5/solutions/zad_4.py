# -*- coding: utf-8 -*-
"""
Created on Mon Dec 28 17:40:03 2020

@author: irena
"""

import matplotlib.image as mpimg
import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt

imageNew = mpimg.imread('D:/rusu_lv_2019_2020/LV5/resources/example_grayscale.png')
    
X = imageNew.reshape((-1, 1)) # We need an (n_sample, n_feature) array
k_means = cluster.KMeans(n_clusters=10,n_init=1)
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
img_compressed = np.choose(labels, values)
img_compressed.shape = imageNew.shape

plt.figure(1)
plt.imshow(imageNew,  cmap='gray')

plt.figure(2)
plt.imshow(img_compressed,  cmap='gray')