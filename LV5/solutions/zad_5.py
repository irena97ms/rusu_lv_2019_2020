# -*- coding: utf-8 -*-
"""
Created on Mon Dec 28 17:48:49 2020

@author: irena
"""

import matplotlib.image as mpimg
import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt

imageNew = mpimg.imread('D:/rusu_lv_2019_2020/LV5/resources/example.png')
 
X = imageNew.reshape((-1, 1))
k_means = cluster.KMeans(n_clusters=10,n_init=1)
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
img_compressed = np.choose(labels, values)
img_compressed.shape = imageNew.shape

plt.figure(1)
plt.imshow(imageNew)

plt.figure(2)
plt.imshow(img_compressed)

height, width = img_compressed.shape[0], img_compressed.shape[1]