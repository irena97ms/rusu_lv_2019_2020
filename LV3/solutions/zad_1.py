# -*- coding: utf-8 -*-
"""
Created on Wed Dec  9 21:45:10 2020

@author: irena
"""

#1.1.

import pandas as pd

cars=pd.read_csv("D:/rusu_lv_2019_2020/LV3/resources/mtcars.csv")
max_cons= cars.sort_values(by=['mpg'], ascending=False)
print("5 automobila s najvecom potrosnjom:")
print(max_cons.head(5))

#1.2.
min_cons = cars.sort_values(by=['mpg'], ascending=True)
min_cons = cars.query("cyl==8")
print("3 automobila s najmanjom potrosnjom:")
print(min_cons.head(3))

#1.3.
sixCyl = cars.query("cyl==6")
avg_cons = sixCyl["mpg"].mean()
print("Srednja potrosnja automobila s 6 cilindara:")
print(avg_cons)

#1.4.
fourCylS = cars.query("cyl==4")
fourCyl = fourCylS.query("wt>=2.000 & wt<=2.200")
avg_cons2=fourCyl["mpg"].mean()
print("Srednja potrosnja automobila s 4 cilindara izmedju 2000 i 2200 lbs:")
print(avg_cons2)

#1.5.
automatic = cars.query("am==0")
manual = cars.query("am==1")
print("Broj automobila s automatskim mjenjacem:")
print(automatic.shape[0])
print("Broj automobila s rucnim mjenjacem:")
print(manual.shape[0])

#1.6.
automatic = cars.query("am==0")
power = automatic.query("hp>100")
print("Broj automobila s automatskim mjenjacem i snagom preko 100 'konja':")
print(power.shape[0])

#1.7.
mass_in_lb = cars["wt"]
mass_in_kg = mass_in_lb*0.453592*1000
print("Mase automobila u kilogramima:")
print(mass_in_kg)