# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 20:52:36 2020

@author: irena
"""

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

slika = mpimg.imread('D:/rusu_lv_2019_2020/LV2/resources/tiger.png')

pod = np.array(slika)
brightness = 5

slika2 = np.multiply(pod, brightness)
slika2 = np.clip(slika2, 0, 1)

imgplot = plt.imshow(slika2) 

plt.show() 
