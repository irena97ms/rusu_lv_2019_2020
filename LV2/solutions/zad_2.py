# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 20:06:58 2020

@author: irena
"""

import re

file = open("D:/rusu_lv_2019_2020/LV2/resources/mbox-short.txt")
min_jedno_a = []
tocno_jedno_a = []
nema_a = []
brojevi = []
mala_slova = []

for line in file:
    line = line.rstrip()
    mail = re.findall('[a-zA-Z0-9._]+@[a-zA-Z0-9._]+', line)
    if mail:
        if mail[0].count('a')>=1:
            min_jedno_a.append(mail[0].split('@')[0])
        if mail[0].count('a')==1:
            tocno_jedno_a.append(mail[0].split('@')[0])
        if mail[0].count('a')==0:
            nema_a.append(mail[0].split('@')[0])
        if re.findall('[0-9]+', mail[0]):
            brojevi.append(mail[0].split('@')[0])
        if mail[0].islower():
            mala_slova.append(mail[0].split('@')[0])

print(min_jedno_a)
print(tocno_jedno_a)
print(nema_a)
print(brojevi)
print(mala_slova)