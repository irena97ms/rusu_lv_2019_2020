# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 20:42:02 2020

@author: irena
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

plt.figure()
auti=pd.read_csv("D:/rusu_lv_2019_2020/LV2/resources/mtcars.csv")
plt.scatter(auti.hp, auti.mpg, c=auti.wt)
plt.colorbar()
plt.ylabel("Potrosnja (mpg)")
plt.xlabel("Konjska snaga (hp)")
print(" Prosjecna potrosnja je: %f\n"%np.average(auti.mpg),"Minimalna potrosnja je: %f\n"%min(auti.mpg),"Maksimalna potrosnja je: %f\n"%max(auti.mpg))
plt.show()
