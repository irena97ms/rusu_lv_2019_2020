# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 20:21:11 2020

@author: irena
"""

import numpy as np
import matplotlib.pyplot as plt

spol = np.random.randint(2, size = 1000)
visine = [0] * 1000
muski = []
zenski = []

for i in range(0,1000):
    if (spol[i] == 1):
        visine[i] = np.random.normal(180,7)
        muski.append(visine[i])
    else:
        visine[i] = np.random.normal(167,7)
        zenski.append(visine[i])

plt.hist([muski,zenski],color=['blue','red'])
plt.legend(["M","Z"])
plt.axvline(np.average(muski),color="black",linewidth=10)
plt.axvline(np.average(zenski),color="purple", linewidth=10)
plt.show()